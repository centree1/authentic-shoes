<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= $title; ?></title>
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>vendors/fa/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>vendors/css/vendor.bundle.addons.css">
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/horizontal-layout/style.css">
  <link rel="shortcut icon" href="<?= base_url('assets/'); ?>images/authentic-shoes2.jpeg" />
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>vendors/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/css/cent.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/sweetalert2/dist/sweetalert2.min.css">
  <!-- datetime -->
  <link rel="stylesheet" href="<?= base_url('assets/vendors/datetimepicker/build/css/bootstrap-datetimepicker.min.css'); ?>">

  <script src="<?= base_url(); ?>assets/vendors/sweetalert2/dist/sweetalert2.min.js"></script>  
  <script src="<?= base_url('assets/'); ?>vendors/js/vendor.bundle.base.js"></script>
  <script src="<?= base_url('assets/'); ?>vendors/js/vendor.bundle.addons.js"></script> 
  <script src="<?= base_url('assets/'); ?>js/dimpul.js"></script>

</head>

<body>

  <div class="container-scroller">
    <!-- partial:partials/_horizontal-navbar.html -->
    <div class="horizontal-menu">
      <nav class="navbar top-navbar col-lg-12 col-12 p-0">
        <div class="nav-top flex-grow-1">
          <div class="container d-flex flex-row h-100 align-items-center">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
              <a class="navbar-brand brand-logo" href="<?= base_url('home'); ?>">
                <img style="height: 50px;" src="<?= base_url('assets/images/authentic-shoes2.jpeg'); ?>">
              </a>
              <!-- <a class="navbar-brand brand-logo-mini" href="index.html"><img src="http://www.urbanui.com/serein/template/images/logo-mini.svg" alt="logo"/></a> -->
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end flex-grow-1">
              <ul class="navbar-nav navbar-nav-right">
                <li class="nav-item dropdown">
                   <!-- <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                    <i class="fa fa-bell  mx-0"></i>
                    <span class="count"></span>
                  </a>
                 <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                    <a class="dropdown-item">
                      <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
                      </p>
                      <span class="badge badge-pill badge-warning float-right">View all</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item preview-item">
                      <div class="preview-thumbnail">
                        <div class="preview-icon bg-success">
                          <i class="fa fa-info-circle mx-0"></i>
                        </div>
                      </div>
                      <div class="preview-item-content">
                        <h6 class="preview-subject font-weight-medium">Application Error</h6>
                        <p class="font-weight-light small-text">
                          Just now
                        </p>
                      </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item preview-item">
                      <div class="preview-thumbnail">
                        <div class="preview-icon bg-warning">
                          <i class="fa fa-cog mx-0"></i>
                        </div>
                      </div>
                      <div class="preview-item-content">
                        <h6 class="preview-subject font-weight-medium">Settings</h6>
                        <p class="font-weight-light small-text">
                          Private message
                        </p>
                      </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item preview-item">
                      <div class="preview-thumbnail">
                        <div class="preview-icon bg-info">
                          <i class="fa fa-user mx-0"></i>
                        </div>
                      </div>
                      <div class="preview-item-content">
                        <h6 class="preview-subject font-weight-medium">New user registration</h6>
                        <p class="font-weight-light small-text">
                          2 days ago
                        </p>
                      </div>
                    </a>
                  </div> -->
                </li>
                <li class="nav-item nav-profile dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                    <img src="<?= base_url('assets/'); ?>images/faces/face5.jpg" alt="profile"/>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                    <a class="dropdown-item" style="cursor: pointer;">
                      <i class="fa fa-user text-primary"></i>
                      Profile
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item"  style="cursor: pointer;" href="<?= base_url('auth/logout'); ?>">
                      <i class=" fa fa-sign-out text-primary"></i>
                      Logout
                    </a>
                  </div>
                </li>
              </ul>
              <button class="btn-menu navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle" >
                <span class="fa fa-reorder"></span>
              </button>
            </div>
          </div>
        </div>
      </nav>
      <nav class="bottom-navbar">
        <div class="container">
          <ul class="nav page-navigation">
            <?php  foreach ($menu as $row) { 
                if($row->uri !== '#'){
                  $uri = " href='".base_url($row->uri)."'";
                }else{
                  $uri = " href='' style='pointer-events: none;' ";
                }
              ?>
              <li class="nav-item">
                <a <?= $uri; ?>class="nav-link" >
                <i class="fa fa-<?= $row->fa_icon; ?> fa-fw"></i>
                <span class="menu-title"><?= $row->nama_menu; ?></span>
                <?php if(!empty($row->child)){ ?><i class="menu-arrow"></i> <?php } ?>
              </a>
                <?php if(!empty($row->child)){ ?>
                    <div class="submenu">
                      <ul class="submenu-item"  style="list-style: none !important;">
                        <?php foreach ($row->child as $key) { ?>
                          <li class="nav-item"><a class="nav-link" href="<?= base_url($key->uri); ?>"> 
                            <i class="fa fa-<?= $key->fa_icon; ?> fa-fw"></i> <?= $key->nama_menu; ?></a>
                          </li>
                        <?php }?>
                      </ul>
                    </div>
                <?php } ?>
              </li>
            <?php  } ?>
          </ul>
        </div>
      </nav>
    </div>  
    <div class="main-panel">
      <div class="content-wrapper">
        <?php 
        if(!empty(get_flashsession())){ ?>
          <div class="alert alert-danger"> <?= get_flashsession(); ?></div>
        <?php } ?>