<?php
function timsar($param){
	#fungsi ini untuk memotong proses dan show param
	ob_get_clean();
    echo "<pre>";
    print_r($param);
    echo "</pre>";
    exit;
}

function post($name){
	$centree = get_instance();
	return $centree->input->post($name);
}

function post_all(){
	$centree = get_instance();
	return $centree->input->post();
}

function get($name){
	$centree = get_instance();
	return $centree->input->get($name);
}

function get_all(){
	$centree = get_instance();
	return $centree->input->get();
}

function timsar_query(){ 
	$centree = get_instance();
	$query = $centree->db->last_query();
	timsar($query);
}

function check_ajax_request(){
	$centree = get_instance();
	if (!$centree->input->is_ajax_request()) {
	   exit('Bad Request');
	}
}

function check_selected($d1, $d2){
	if($d1==$d2){
		return "selected";
	}
}

function check_checked($d1, $d2){
	if($d1==$d2){
		return "checked";
	}
}

function check_session(){
	$centree = get_instance();
	$centree->load->library('session');
	if(empty($centree->session->userdata('id_user'))){
		redirect(base_url());
	}
}
function get_session($sess){
	$centree = get_instance();
	return $centree->session->userdata($sess);
}

function timsar_session(){ 
	$centree = get_instance();
	timsar($centree->session->userdata());
}

function return_ajax($status,$text){
  $return = array('stt'=>$status,'message'=>$text);
  echo json_encode($return);
  exit;
}

function checking_empty($data,$return_ajax=true){
  foreach ($data as $row) {
      if(empty($row)){
      	if($return_ajax==true){
        	return_ajax(0,"Maaf, Data Tidak Lengkap");
      	}else{
      		echo '<div class="alert alert-danger"><i class="fa fa-close"> </i> Maaf, Data Tidak Lengkap</div>';
      		exit;
      	}
      }
  }
}

function set_flashsession($text){
	$centree = get_instance();
	$centree->session->set_flashdata('flash', $text);
}

function get_flashsession(){
	$centree = get_instance();
	return $centree->session->flashdata('flash');
}

function granted_access(){
	$centree = get_instance();
	$uri_all = $centree->uri->uri_string;
	$uri = $centree->uri->segment(1);

	//get role
	$centree->db->select("A.role_id");
	$centree->db->from("menu_akses_tb A");
	$centree->db->join("menu_tb B","A.id_menu = B.id_menu");
	$centree->db->where("B.uri",$uri);
	$centree->db->or_where("B.uri",$uri_all);
	$data = $centree->db->get()->result();
	$role = array();
	if(empty($data)){
		set_flashsession('<i class="fa fa-close"> </i> Akses Ditolak');
		redirect(base_url('home'));
	}else{
		foreach ($data as $row) {
			array_push($role, $row->role_id);
		}
	}
	$sess_role = get_session('role');
	if(!in_array($sess_role, $role)){
		set_flashsession('<i class="fa fa-close"> </i> Akses Ditolak');
		redirect(base_url('home'));
	}
}

function active($stat){
	if($stat==1){
		return "<div class='badge badge-success'> Aktif </div>";
	}else{
		return "<div class='badge badge-danger'> Tidak Aktif </div>";
	}
}

function bulan($i){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);

	return $bulan[$i];
}

function tahun_bulan($tanggal){
	$arr = explode('-', $tanggal);
	$tahun = $arr[0];
	$bulan = bulan((int)$arr[1]);
	return $bulan." ".$tahun;
}

function time_to_second($time){ 
  $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time);

  sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds); 
  $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;

  return $time_seconds;
}

function second_to_time($second){
  return gmdate("H:i:s", $second);
}

?>

