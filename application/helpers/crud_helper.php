<?php

function h_crud_get_data($nama_table,$where=array(),$order_by='',$order='ASC'){
	#fungsi ini digunakan untuk get semua data
	$centree = get_instance();

	if(!empty($order_by)){
		$centree->db->order_by($order_by,$order);
	}
	return $centree->db->get_where($nama_table,$where)->result();
}

function h_crud_get_1_data($nama_table,$where=array()){
	#fungsi ini digunakan untuk get 1 data
	$centree = get_instance();
	return $centree->db->get_where($nama_table,$where)->row();
}

function h_crud_tambah($nama_table, $data){
	#fungsi ini digunakan untuk tambah ke database
	$centree = get_instance();
	return $centree->db->insert($nama_table, $data);
}

function h_crud_tambah_batch($nama_table, $data){
	#fungsi ini digunakan untuk tambah ke database (batch)
	$centree = get_instance();
	return $centree->db->insert_batch($nama_table, $data);
}

function h_crud_update($nama_table,$data,$nama_kolom_key,$key){
	$centree = get_instance();
	$centree->db->where($nama_kolom_key, $key);
	return $centree->db->update($nama_table, $data);
}

function h_crud_delete($nama_table,$nama_kolom_key,$key){
	$centree = get_instance();
	$centree->db->where($nama_kolom_key, $key);
	if($centree->db->delete($nama_table)){
		return 1;
	}else{
		return 0;
	}
}

function h_crud_delete_ver2($nama_table,$where){
	$centree = get_instance();
	if($centree->db->delete($nama_table, $where)){
		return 1;
	}else{
		return 0;
	}
}


?>
