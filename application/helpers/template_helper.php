<?php
function load_view_main($view,$data=null,$title='Authentic Shoes'){
  //design karma
  $centree = get_instance();
  $header['titlexyz'] = $title;
  //$header['menu'] = get_menu(1);
  $centree->load->view('main_header',$header);
    $centree->load->view($view, $data);
    $centree->load->view('main_footer');
}

function load_view($view,$data=null,$title='Authentic Shoes'){
	//design halaman setelah login
	$centree = get_instance();
    //check_session();
    //granted_access();
	ob_start();
	$header['title'] = $title;
	$header['menu'] = get_menu(get_session('role'));
	//$header['menu'] = get_menu();
	$centree->load->view('template/header',$header);
    $centree->load->view($view,$data);
    $centree->load->view('template/footer');
}

function load_view_login($view,$data=null,$title='Authentic Shoes'){
	//design halaman setelah login
	$centree = get_instance();
	ob_start();
	$header['title'] = "AS - ".$title;
	$header['menu'] = get_menu(1);
	$centree->load->view('template/header_login',$header);
    $centree->load->view($view,$data);
    $centree->load->view('template/footer_login');
}


function load_empty_view($view,$data=null){
    //check_session();
	$centree = get_instance();
	ob_start();
    $centree->load->view($view,$data);
}

function get_menu($role){
	$centree = get_instance();

	//get menu
	$centree->db->select('A.*');
	$centree->db->from('menu_tb A');
	$centree->db->join('menu_akses_tb B','A.id_menu = B.id_menu');
	$centree->db->where('B.role_id',$role);
	$centree->db->order_by('A.nama_menu','ASC');
	$data = $centree->db->get()->result();

	$menu = array();
	foreach ($data as $key => $row) {
		$parent = $row->parent;
		if($parent == 0){
			$id_menu = $row->id_menu;
			$menu[$id_menu] = $row;
			$menu[$id_menu]->child = [];
			unset($data[$key]);
		}
	}

	foreach ($data as $key => $row) {
		$parent = $row->parent;
		array_push($menu[$parent]->child, $row);
	}
	return $menu;
}


?>