<?php
	#inisialisasi
  $id_master_barang  = @$master->id_master_barang;
  $nama_barang       = @$master->nama_barang;
  $harga             = @$master->harga;
  $kategori          = @$master->kategori;
  $merek             = @$master->merek;
  $keterangan        = @$master->keterangan;
  $foto              = @$master->foto;
?>      
<form id="global-form">
  <div class="form-group">
    <label>Nama Master Barang</label>
    <input class="form-control" name="nama_barang" minlength="2" type="text" value="<?= $nama_barang; ?>" required>
  </div>
  <div class="form-group ">
    <label>Harga</label>
    <div class="input-group">
    <input class="form-control" name="harga" type="text" value="<?= $harga; ?>">
    </div>
  </div>

  </div>
  <div class="form-group"> 
    <label>Kategori</label>
    <div class="input-group">
    <input class="form-control" name="kategori" type="text" value="<?= $kategori; ?>">
    </div>
  </div>
    <div class="form-group"> 
    <label>Merek</label>
    <div class="input-group">
    <input class="form-control" name="merek" type="text" value="<?= $merek; ?>">
    </div>
  </div>
  <div class="form-group"> 
    <label>Keterangan</label>
    <div class="input-group">
    <input class="form-control" name="keterangan" type="text" value="<?= $keterangan; ?>">
    </div>
  </div>
    <div class="form-group"> 
    <label>Foto</label>
    <div class="input-group">
    <input class="form-control" name="foto" type="text" value="<?= $foto; ?>">
    </div>
  </div>
  <?php
	if($type=="update"){
		$url = base_url('master_barang/update_conf'); ?>
		 <div class="form-group cent-hidden">
		    <label>ID</label>
		    <input class="form-control" value="<?= $id_master_barang; ?>" name="id" minlength="2" type="text" required>
		  </div>
	<?php }else{
		$url = base_url('master_barang/tambah_conf');
	}

?>
  <center><input id="submit-btn" class="btn btn-primary" type="submit" value="Submit"></center>
</form>

<script type="text/javascript">
    $('.tgl').datetimepicker({
          format: 'HH:mm',
          useCurrent: true,
          widgetPositioning: {
            horizontal: 'left',
            vertical: 'top'
        }
      });
  </script>

<script type="text/javascript">
   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax(values,"<?= $url; ?>","","Berhasil!","Gagal!","submit-btn","button");
        return false; //stop
    });
</script>