
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-7 cent-left"><h3 class="">Master Barang</h3></div>
                <div class="col-md-5 cent-right" id="btn_config">
                    <button class="btn btn-success btn-sm clickable_row_button" id="tambah"  data-toggle="tooltip" data-placement="bottom" title="Tambah"><i class="fa fa-plus"></i></button>
                    <button class="btn btn-warning btn-sm cent-hidden clickable_row_button" id="update"  data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-sm cent-hidden clickable_row_button" id="delete"  data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-close"></i></button>
                </div>
            </div>
              
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table table table-hover datatable">
                      <thead>
                        <tr>
                            <th>ID Master Barang</th>
                            <th>Nama Barang</th>
                            <th>Harga</th>
                            <th>Kategori</th>
                            <th>Merek</th>
                            <th>Keterangan</th>
                            <th>Foto</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($list as $row) { ?>
                            <tr class="clickable_row" data-id="<?= $row->id_master_barang; ?>" class="menu">
                                <td><?= $row->id_master_barang; ?></td>
                                <td><?= $row->nama_barang; ?></td>
                                <td><?= $row->harga; ?></td>
                                <td><?= $row->kategori; ?></td>
                                <td><?= $row->merek; ?></td>
                                <td><?= $row->keterangan; ?></td> 
                                <td><?= $row->foto; ?></td>   
                            </tr>
                        <?php }  ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#tambah").click(function(){
        get_append_ajax("type=tambah", "<?= base_url('master_barang/get_form_master'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-plus'></i> Tambah Master Absen");
    });


    $("#delete").click(function(){
           pesan_confirm("Apakah anda yakin?", "Menghapus Master Barang id:"+tr_id, "Ya, Hapus").then((result) => {
            if(result===true){
                simple_ajax('id='+tr_id,"master_barang/delete_conf","","Berhasil!","Gagal!","div-alert","div");
            }
        });
      });

    $("#update").click(function(){
        get_append_ajax("type=update&id="+tr_id, "<?= base_url('master_barang/get_form_master'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-pencil'></i> Edit Master Barang") 
      });

  });
</script>