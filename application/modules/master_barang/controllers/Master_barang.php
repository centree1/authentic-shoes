
<?php
  #inisialisasi
  $id_master_barang  = @$master->id_master_barang;
  $nama_barang       = @$master->nama_barang;
  $harga             = @$master->harga;
  $kategori          = @$master->kategori;
  $merek             = @$master->merek;
  $keterangan        = @$master->keterangan;
  $foto              = @$master->foto;
?> <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_barang extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      $this->load->library('password');
    }


    public function index()
    {
       #Bagian Inisialisasi
        $data_master = array();
        
      #Bagian Proses
        $data_master = h_crud_get_data('master_barang_tb');
        
      #Bagian Return
        $data['list']   = $data_master;
        load_view('list',$data,'List Master Absen');
    }

    public function get_form_master()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $data_checking_empty = array($id);
            checking_empty($data_checking_empty);
            $where      = array("id_master_barang"=>$id);
            $data_master  = h_crud_get_1_data("master_barang_tb",$where);

            if(empty($data_master)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_master  = null;
         }


      #Bagian Return
         $data['master'] = $data_master;
         $data['type'] = $type;
         load_empty_view('form_barang',$data);
    }

    public function get_form_password()
    {  
      #Bagian Inisialisasi
      #Bagian Proses
      #Bagian Return
         load_empty_view('form_password');
    }

     public function tambah_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $nama_barang       = post('nama_barang');
        $harga             = post('harga');
        $kategori          = post('kategori');
        $merek             = post('merek');
        $keterangan        = post('keterangan');
        $foto              = post('foto');


        $data_checking_empty = array($nama_barang,$harga,$kategori,$merek,$keterangan,$foto);
        checking_empty($data_checking_empty);

      #Bagian Proses
        //eksekusi guys
        $data = array(
          "nama_barang"=>$nama_barang,
          "harga"=>$harga,
          "kategori"=>$kategori,
          "merek"=>$merek,
          "keterangan"=>$keterangan,
          "foto"=>$foto
        );
        $execute = h_crud_tambah('master_barang_tb', $data);

      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menambah Master Barang");
        }else{
          return_ajax(0,"Gagal Menambah Master Barang");
        }
    }

    

    public function delete_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
        $data_checking_empty = array($id);
        checking_empty($data_checking_empty);

      #Bagian Proses
        if($id==1){
          return_ajax(0,"Maaf, Master Absen OFF tidak dapat dihapus");
        }

        $execute = h_crud_delete('master_barang_tb','id_master_barang',$id);
        
      #Bagian Return
        if(!$execute){
          return_ajax(0,"Gagal Menghapus Master Barang");
        }else{
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menghapus Master Barang");
        }
    }

    public function update_conf(){
       #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $nama_barang   = post('nama_barang');
        $harga         = post('harga');
        $kategori      = post('kategori');
        $merek         = post('merek');
        $harga         = post('harga');
        $keterangan    = post('keterangan');
        $foto          = post('foto');
        $id            = post('id');
        $data_checking_empty = array($nama_barang,$harga,$kategori,$merek,$keterangan,$foto,$id);
        checking_empty($data_checking_empty);

      #Bagian Proses 
        //eksekusi guys
        $data = array(
          "nama_barang"=>$nama_barang,
          "harga"=>$harga,
          "kategori"=>$kategori,
          "merek"=>$merek,
          "keterangan"=>$keterangan,
          "foto"=>$foto
        );
        $execute = h_crud_update('master_barang_tb',$data,'id_master_barang',$id);
        
      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Mengubah Master Barang");
        }else{
          return_ajax(0,"Gagal Mengubah Master Barang");
        }
        echo json_encode($return);
    }
}