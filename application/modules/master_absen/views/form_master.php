<?php
	#inisialisasi
  $id_master_absen  = @$master->id_master_absen;
  $nama             = @$master->nama_master_absen;
  $jam_masuk        = @$master->jam_masuk;
  $jam_pulang       = @$master->jam_pulang;
?>
<form id="global-form">
  <div class="form-group">
    <label>Nama Master Absen</label>
    <input class="form-control" name="nama_master_absen" minlength="2" type="text" value="<?= $nama; ?>" required>
  </div>
  <div class="form-group ">
    <label>Jam Masuk</label>
    <div class="input-group">
    <input class="form-control tgl" name="jam_masuk" type="text" value="<?= $jam_masuk; ?>">
    </div>
  </div>

  </div>
  <div class="form-group"> 
    <label>Jam Pulang</label>
    <div class="input-group">
    <input class="form-control tgl" name="jam_pulang" type="text" value="<?= $jam_pulang; ?>">
    </div>
  </div>

  <?php
	if($type=="update"){
		$url = base_url('master_absen/update_conf'); ?>
		 <div class="form-group cent-hidden">
		    <label>ID</label>
		    <input class="form-control" value="<?= $id_master_absen; ?>" name="id" minlength="2" type="text" required>
		  </div>
	<?php }else{
		$url = base_url('master_absen/tambah_conf');
	}

?>
  <center><input id="submit-btn" class="btn btn-primary" type="submit" value="Submit"></center>
</form>

<script type="text/javascript">
    $('.tgl').datetimepicker({
          format: 'HH:mm',
          useCurrent: true,
          widgetPositioning: {
            horizontal: 'left',
            vertical: 'top'
        }
      });
  </script>

<script type="text/javascript">
   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax(values,"<?= $url; ?>","","Berhasil!","Gagal!","submit-btn","button");
        return false; //stop
    });
</script>