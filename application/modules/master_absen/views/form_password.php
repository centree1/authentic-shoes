
<form id="global-form">
  <div class="form-group">
    <label>Password Baru</label>
    <input class="form-control" name="password" minlength="6" type="password" required>
    <small class="text-success"> Password minimal 6 karakter</small>
  </div>
  <center><input id="submit-btn" class="btn btn-primary" type="submit" value="Submit"></center>
</form>

<script type="text/javascript">

   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax(values+"&id="+tr_id,"<?= base_url('user/password_conf'); ?>","","Berhasil!","Gagal!","submit-btn","button");
        return false; //stop
    });
</script>