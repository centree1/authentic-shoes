<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NamaModule extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
    }

    public function index()
    {
       echo "xyz";
    }

    public function standarisasi_fungsi()
    {  
      #Bagian Inisialisasi
    	//isi
      #Bagian Proses
    	//isi
      #Bagian Return
    	//isi
    }

    public function contoh_transaction(){
		$this->db->trans_start();
		$query1 = $this->db->query('AN SQL QUERY...');
		$query2 = $this->db->query('ANOTHER QUERY...');
		$this->db->trans_complete();

		/*
			jika query 2 gagal maka query 1 akan dibatalkan
		*/
    }


}