<style type="text/css">
  .auth .login-half-bg {
    background: url("<?= base_url(); ?>assets/images/auth/bg2.jpg");
    background-size: cover;
  }
  @media (max-width: 1000px) {
  .login-half-bg {
    display: none !important;
  }
}
</style>

  <div class="container-scroller" style="">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
        <div class="row flex-grow" style="top: 0px;">

          <div class="col-lg-6 login-half-bg d-flex flex-row" style="">
            <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2020 Centree</p>
          </div>
          <div class="col-lg-6 d-flex align-items-center justify-content-center">
            <div class="auth-form-transparent text-left p-3">
              <div class="">
                <img style="width: 300px; height: auto;" src="<?= base_url('assets/'); ?>images/logo.png">
              </div>
              
              <form class="pt-3" id="frmlogin">

                <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
                <div class="form-group">
                  <label for="">Email / Username</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="fa fa-user text-primary"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control form-control-lg border-left-0" name="username" placeholder="Username" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="">Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="fa fa-key text-primary"></i>
                      </span>
                    </div>
                    <input type="password" class="form-control form-control-lg border-left-0"  placeholder="Password" name="password" required="">                        
                  </div>
                </div>
                <div class="my-3">
                  <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">LOGIN</a>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  Lupa Password? <button  class="btn btn-info btn-sm" type="button" onclick="frgtpwd()">Klik Disini</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
 
 <script type="text/javascript">
  function frgtpwd(){
    $("#div-alert").removeClass('alert-danger');
    $("#div-alert").addClass('alert-warning');
    $("#div-alert").empty();
    $("#div-alert").append("<i class='fa fa-cog fa-spin'></i> Mohon maaf, Fitur Belum Tersedia");
    $("#div-alert").fadeIn();
  }

   $('#frmlogin').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: "<?= base_url('auth/conf'); ?>",
            data: values,
            beforeSend: function(){
                $("#div-alert").removeClass('alert-danger');
                $("#div-alert").addClass('alert-warning');
                $("#div-alert").empty();
                $("#div-alert").append("<i class='fa fa-spin fa-spinner'></i> Mohon Tunggu . . .");
                $("#div-alert").fadeIn();
            },
            success: function (data) {
                //$("#div-alert").fadeOut();
                if(data==""){
                  $("#div-alert").removeClass('alert-warning');
                  $("#div-alert").addClass('alert-danger');
                  $("#div-alert").empty();
                  $("#div-alert").append("<i class='fa fa-close'></i> Data Tidak Ditemukan");
                  $("#div-alert").fadeIn();
                  return 0;
                }

                var returnx = JSON.parse(data);
                if(returnx.stt == '1'){
                  $("#div-alert").fadeIn();
                  window.location ="<?= base_url('home'); ?>";
                }else{
                  $("#div-alert").removeClass('alert-warning');
                  $("#div-alert").addClass('alert-danger');
                  $("#div-alert").empty();
                  $("#div-alert").append("<i class='fa fa-close'></i> "+returnx.message);
                  $("#div-alert").fadeIn();
                  return 0;
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                pesan_error("Gagal!", errorThrown);
            }
        });
    });
 </script>