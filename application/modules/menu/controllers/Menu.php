<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	public function __construct()
    {
	    parent::__construct();
    }

    public function index()
    {
      #Bagian Inisialisasi
        $data_menu = array();
        
      #Bagian Proses
        $where          = array('parent'=>0);
        $data_menu = h_crud_get_data('menu_tb',$where);

      #Bagian Return
        $data['list_menu'] = $data_menu;
        load_view('list',$data,'List Menu');
    }

    public function get_sub_menu()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $id_parent      = $this->input->post('id_parent');

      #Bagian Proses
        $where          = array('parent'=>$id_parent);
        $data_sub_menu  = h_crud_get_data('menu_tb',$where);

      #Bagian Return
        $data['sub_menu'] = $data_sub_menu;
        load_empty_view('list_sub_menu',$data);
    }

    public function get_update()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $id             = $this->input->post('id');

      #Bagian Proses
        $where          = array('id_menu'=>$id);
        $data_menu      = h_crud_get_1_data('menu_tb',$where);
        $str_data_menu  = json_encode($data_menu);

      #Bagian Return
        $return = array('stt'=>1,'message'=>$str_data_menu);
        echo json_encode($return);
    }

    public function tambah_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $nama_menu      = $this->input->post('nama_menu');
        $uri            = $this->input->post('uri');
        $fa_icon        = $this->input->post('fa_icon');
        $parent         = $this->input->post('parent');

        if(empty($parent)){
          $parent = 0;
        }
        $data = array(
          "nama_menu"=>$nama_menu,
          "uri"=>$uri,
          "fa_icon"=>$fa_icon,
          "parent"=>$parent
        );
      #Bagian Proses
        $execute = h_crud_tambah('menu_tb', $data);
      #Bagian Return
        if($execute){
          $return = array('stt'=>1,'message'=>'Berhasil Menambah Menu');
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Menambah Menu');
        }

        $this->db->trans_complete();
        echo json_encode($return);
    }

    public function update_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id_menu');
        $nama_menu      = $this->input->post('nama_menu');
        $uri            = $this->input->post('uri');
        $fa_icon        = $this->input->post('fa_icon');
        $data = array(
          "nama_menu"=>$nama_menu,
          "uri"=>$uri,
          "fa_icon"=>$fa_icon
        );
      #Bagian Proses
        $execute = h_crud_update('menu_tb',$data,'id_menu',$id);
      #Bagian Return
        if($execute){
          $return = array('stt'=>1,'message'=>'Berhasil Update Menu');
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Update Menu');
        }

        $this->db->trans_complete();
        echo json_encode($return);
    }

    public function delete_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
      #Bagian Proses
        $execute = h_crud_delete('menu_tb','id_menu',$id);
        if(!$execute){
          $return = array('stt'=>0,'message'=>'Gagal Menghapus Menu');
        }

        $execute = h_crud_delete('menu_tb','parent',$id);
        if(!$execute){
          $return = array('stt'=>0,'message'=>'Gagal Menghapus Sub Menu');
        }

        $return = array('stt'=>1,'message'=>'Berhasil Menghapus Menu & Sub Menu');

      #Bagian Return
        $this->db->trans_complete();
        echo json_encode($return);
    }

    public function get_access()
    {
      #Bagian Keterangan
       /* 
          1. Ambil role menu akses
          2. jadikan array, array role apa saja yang terdapat pada menu ini
          3. lakukan pengecekan untuk menampilkan sole apa saja yang diperbolehkan akses menu tsb
          4. tampilkan form dengan logic jika akses menu sudah ada maka checked
       */

      #Bagian Inisialisasi
        check_ajax_request();
        $id             = $this->input->post('id_menu');
        $tmp_akses      = array();

      #Bagian Proses
        $where            = array('id_menu'=>$id);
        $data_role        = h_crud_get_data('role_tb');
        $data_menu_akses  = h_crud_get_data('menu_akses_tb',$where);
        foreach ($data_menu_akses as $row) {
           array_push($tmp_akses, $row->role_id);
        }

        foreach ($data_role as $row) {
           if(in_array($row->id_role,$tmp_akses)){
              $row->checked = 1;
           }else{
              $row->checked = 0;
           }
        }

      #Bagian Return
        $data['role']    = $data_role;
        $data['id_menu'] = $id;
        load_empty_view('form_menu_akses',$data);
    }

    public function access_conf()
    {
      #Bagian Keterangan
       /* 
          1. Hapus semua akses pada menu tb
          2. masukan data akses yang baru di submit
       */

      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id       = $this->input->post('id_menu');
        $role     = $this->input->post('role[]');
        $tmp_role = array();

      #Bagian Proses
        $execute  = h_crud_delete('menu_akses_tb','id_menu',$id);
        if(!$execute){
          $return = array('stt'=>0,'message'=>'Gagal Membuat Akses Menu');
          echo json_encode($return);
          return 0;
        }

        foreach ($role as $row) {
            array_push($tmp_role, array("role_id"=>$row, "id_menu"=>$id));
        }

        $execute = h_crud_tambah_batch('menu_akses_tb', $tmp_role);
        if($execute){
          $return = array('stt'=>1,'message'=>'Berhasil Menambahkan Menu Akses');
          $this->db->trans_complete();
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Menambahkan Akses Menu');
        }

      #Bagian Return
        echo json_encode($return);
    }
}