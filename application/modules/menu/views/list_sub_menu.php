<?php foreach ($sub_menu as $row) { ?>

  <tr id="tr-sub-<?= $row->id_menu; ?>" class='submenu' onclick="click_sub_menu('<?= $row->id_menu; ?>','submenu')" class="menu">
        <td><?= $row->nama_menu; ?></td>
        <td><?= $row->uri; ?></td>
        <td><i class="fa fa-2x fa-fw fa-<?= $row->fa_icon; ?>"></i></td>
    </tr>

<?php } ?>

<script type="text/javascript">

  function click_sub_menu(id_menu,type){
        $(".submenu").removeClass("selected");
        $("#tr-sub-"+id_menu).addClass("selected");
        id = id_menu;
        $("#btn_config").addClass("show");
        $("#update2").removeClass("cent-hidden");
        $("#delete2").removeClass("cent-hidden");
        $("#access2").removeClass("cent-hidden");
    }

    $("#tambah2").click(function(){
       action = '<?= base_url("menu/tambah_conf"); ?>';
       var title = '<i class="fa fa-plus"></i> Tambah Menu';
       set_label("ModalLabel",title);
       var data = [
         {"label":"Parent","name":"parent","value":parent, "else":"style='display:none;'"},
         {"label":"Nama Menu","name":"nama_menu","value":"", "else":""},
         {"label":"URI","name":"uri","value":"", "else":""},
         {"label":"FA Icon","name":"fa_icon","value":"", "else":""}
       ];
       var form = generate_form(data);
       set_label("ModalBody",form);
       $("#modal_tambah").modal('show');
    });

    $("#access2").click(function(){
      $("#access").click();
    });

    $("#update2").click(function(){
      $("#update").click();
    });

    $("#delete2").click(function(){
           action = '<?= base_url("menu/delete_conf"); ?>';
           pesan_confirm("Apakah anda yakin?", "Menghapus Sub Menu?", "Ya, Hapus").then((result) => {
            if(result===true){
                simple_ajax('id='+id,action);
            }
        });
        });

</script>