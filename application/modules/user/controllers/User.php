<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      $this->load->model('m_user');
      $this->load->library('password');
    }

    public function index()
    {
       #Bagian Inisialisasi
        $data_user = array();
        
      #Bagian Proses
        $data_user = $this->m_user->get_user();
        
      #Bagian Return
        $data['list']   = $data_user;
        load_view('list',$data,'List User');
    }

    public function get_form_user()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $where      = array("id_user"=>$id);
            $data_user  = h_crud_get_1_data("user_tb",$where);

            if(empty($data_user)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_user  = null;
         }

         $data_role_user = $this->m_user->get_role_user();

      #Bagian Return
         $data['user'] = $data_user;
         $data['type'] = $type;
         $data['role'] = $data_role_user;
         load_empty_view('form_user',$data);
    }

    public function get_form_password()
    {  
      #Bagian Inisialisasi
      #Bagian Proses
      #Bagian Return
         load_empty_view('form_password');
    }

     public function tambah_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $name      = post('name');
        $email     = post('email');
        $username  = post('username');
        $role      = post('role');

      #Bagian Proses
        //generate password default
        $password  =  $this->password->custom_hash('654321');

        //eksekusi guys
        $data = array(
          "nama"=>$name,
          "email"=>$email,
          "username"=>$username,
          "role"=>$role,
          "password"=>$password
        );
        $execute = h_crud_tambah('user_tb', $data);

      #Bagian Return
        if($execute){
          $return = array('stt'=>1,'message'=>'Berhasil Menambah User');
          $this->db->trans_complete();
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Menambah User');
        }
        echo json_encode($return);
    }

    public function cek_email()
    {  
      #Bagian Inisialisasi
         $email = post('email');

      #Bagian Proses
         $where      = array("email"=>$email);
         $data_email = h_crud_get_1_data("user_tb",$where);
         if(empty($data_email)){
            $return = '<small class="text-success"> E-Mail Dapat Digunakan </small>';
            $return .= '<script> $("#submit-btn").removeAttr("disabled");</script>';
         }else{
            $return = '<small class="text-danger"> E-Mail Tidak Dapat Digunakan </small>';
            $return .= '<script> $("#submit-btn").attr("disabled","disabled");</script>';
         }

      #Bagian Return
         echo $return;
    }

    public function cek_username()
    {  
      #Bagian Inisialisasi
         $username = post('username');

      #Bagian Proses
         $where      = array("username"=>$username);
         $data       = h_crud_get_1_data("user_tb",$where);
         if(empty($data)){
            $return = '<small class="text-success"> Username Dapat Digunakan </small>';
            $return .= '<script> $("#submit-btn").removeAttr("disabled");</script>';
         }else{
            $return = '<small class="text-danger"> Username Tidak Dapat Digunakan </small>';
            $return .= '<script> $("#submit-btn").attr("disabled","disabled");</script>';
         }

      #Bagian Return
         echo $return;
    }

    public function delete_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
      #Bagian Proses
        $execute = h_crud_delete('user_tb','id_user',$id);
        if(!$execute){
          $return = array('stt'=>0,'message'=>'User memiliki history keuangan');
        }else{
          $this->db->trans_complete();
          $return = array('stt'=>1,'message'=>'Berhasil Menghapus User');
        }
        
      #Bagian Return
        echo json_encode($return);
    }

    public function update_conf(){
       #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $name      = post('name');
        $email     = post('email');
        $username  = post('username');
        $role      = post('role');
        $id        = post('id');

      #Bagian Proses 

        //eksekusi guys
        $data = array(
          "nama"=>$name,
          "email"=>$email,
          "username"=>$username,
          "role"=>$role,
        );
        $execute = h_crud_update('user_tb',$data,'id_user',$id);

      #Bagian Return
        if($execute){
          $return = array('stt'=>1,'message'=>'Berhasil Update User');
          $this->db->trans_complete();
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Update User');
        }
        echo json_encode($return);
    }

    public function password_conf(){
       #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $password  = post('password');
        $id_user   = post('id');

      #Bagian Proses 
        $pwd = $this->password->custom_hash($password);
        $data = array(
          "password"=>$pwd
        );
        $execute = h_crud_update('user_tb',$data,'id_user',$id_user);

      #Bagian Return
        if($execute){
          $return = array('stt'=>1,'message'=>'Berhasil Update Password');
          $this->db->trans_complete();
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Update Password');
        }
        echo json_encode($return);
    }

}