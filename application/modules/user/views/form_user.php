<?php
	#inisialisasi
	$id_user	= @$user->id_user;
	$nama 		= @$user->nama;
	$email 		= @$user->email;
	$username 	= @$user->username;
	$status		= @$user->status_akun;
	$rolex 		= @$user->role;
?>
<form id="global-form">
  <div class="form-group">
    <label>Nama Lengkap</label>
    <input class="form-control" name="name" minlength="2" type="text" value="<?= $nama ?>" required>
  </div>
  <div class="form-group"> 
    <label>E-Mail</label>
    <input class="form-control" name="email" type="email" id="email" value="<?= $email ?>">
    <small id="imel"></small>
  </div>
  <div class="form-group">
    <label>Username</label>
    <input class="form-control" id="username" name="username" minlength="5" type="text" value="<?= $username ?>" required>
    <small id="usrnm"></small>
  </div>
  <div class="form-group">
    <label>Role</label>
    <select class="form-control select2" name="role" style="width: 100%;" required="">
    	<option value=""> -- Pilih User Role --</option>
    	<?php foreach($role as $row) { ?>
    		<option value="<?= $row->id_role; ?>" <?= check_selected($row->id_role, $rolex); ?>> <?= $row->nama_role; ?></option>
    	<?php } ?>
    </select>
  </div>

  <?php
	if($type=="update"){
		$url = base_url('user/update_conf'); ?>
		 <div class="form-group cent-hidden">
		    <label>ID</label>
		    <input class="form-control" value="<?= $id_user; ?>" name="id" minlength="2" type="text" required>
		  </div>
	<?php }else{
		$url = base_url('user/tambah_conf');
	}

?>
  <center><input id="submit-btn" class="btn btn-primary" type="submit" value="Submit"></center>
</form>

<script type="text/javascript">
   $('.select2').select2({
   	theme: "bootstrap"
   });

   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax(values,"<?= $url; ?>","","Berhasil!","Gagal!","submit-btn","button");
        return false; //stop
    });

   $( "#email" ).change(function() {
   	  var email = $(this).val();
   	  console.log(email);
   	  get_append_ajax("email="+email, '<?= base_url("user/cek_email"); ?>', "imel", "submit-btn","button");
	});

   $( "#username" ).change(function() {
   	  var username = $(this).val();
   	  console.log(email);
   	  get_append_ajax("username="+username, '<?= base_url("user/cek_username"); ?>', "usrnm", "submit-btn","button");
	});
</script>