<?php
class M_user extends CI_Model {
   public function __construct()
   {
        parent::__construct();
   }

   public function get_user()
   {
        #get role without karyawan and super user
        $role = array(1);
        $this->db->select('*');
        $this->db->from('user_tb a');
        $this->db->join('role_tb b','b.id_role = a.role');

        $this->db->where_not_in('a.role',$role);
        return $this->db->get()->result();
   }

   public function get_role_user()
   {
        #get role without karyawan and super user
        $role = array(1);
        $this->db->select('*');
        $this->db->from('role_tb');
        $this->db->where_not_in('id_role',$role);
        return $this->db->get()->result();
   }

   
}

?>