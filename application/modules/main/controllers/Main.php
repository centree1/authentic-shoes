<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MX_Controller {

    public function index()
    {
    	load_view_main('index');
    }

     public function katalog()
    {
    	load_view_main('katalog');
    }
}