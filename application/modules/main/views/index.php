<style>
/* 
when .item:hover, show overlay.
overwrite .item-overlay."position"
*/
.item:hover .item-overlay.top {
  top: 0;
}
.item:hover .item-overlay.right {
  right: 0;
  left: 0;
}
.item:hover .item-overlay.bottom {
  bottom: 0;
}
.item:hover .item-overlay.left {
  left: 0;
}

/* 
by default, overlay is visible… 
*/
.item-overlay {
  position: absolute;
  top: 0; right: 0; bottom: 0; left: 0;
  
  background: rgba(0,0,0,0.5);
  color: #fff;
  overflow: hidden;
  text-align: center;
  /* fix text transition issue for .left and .right but need to overwrite left and right properties in .right */
  width: 100%; 
  
  -moz-transition: top 0.3s, right 0.3s, bottom 0.3s, left 0.3s;
  -webkit-transition: top 0.3s, right 0.3s, bottom 0.3s, left 0.3s;
  transition: top 0.3s, right 0.3s, bottom 0.3s, left 0.3s;
}
/*
…but this hide it
*/
.item-overlay.top {
  top: 100%;
}
.item-overlay.right {
  right: 200%;
  left: -100%;
}
.item-overlay.bottom {
  bottom: 100%;
}
.item-overlay.left {
  left: 100%;
}


/* misc. CSS */
* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

.item {
  position: relative;
  
  
  float: left;

  overflow: hidden;

  max-width: 540px;
}
.item img {
  max-width: 100%;
}
/* end of misc. CSS */
</style>


<!-- Banner -->
<section class="banner-area">
<div class="container">
<div class="row fullscreen align-items-center justify-content-start">
<div class="col-lg-12">
<div class="active-banner-slider owl-carousel">

<div class="row single-slide align-items-center d-flex">
<div class="col-lg-5 col-md-6">
<div class="banner-content">
<!-- <h1 style="color: white !important; padding-top: 350px;">AUTHENTIC<br>SHOES</h1> -->
<h1 style="color: white !important; padding-right: 500px;">AUTHENTIC SHOES</h1>
<!-- <p>isi sub konten</p> -->
<p></p>
<!-- <div class="add-bag d-flex align-items-center">
<a class="add-btn" href="#"><span class="lnr lnr-cross"></span></a>
<span class="add-text text-uppercase" style="color: white !important;">Add to Bag</span>
</div> -->
<a href="#" class="primary-btn">SHOP NOW</a></div>
</div>
</div>
</div>
<!-- <div class="col-lg-7">
<div class="banner-img">
<img class="img-fluid" src="<?php echo base_url(); ?>assets/template-depan/img/banner/banner-img.png" alt="">
</div>
</div> 
</div>

<div class="row single-slide">
<div class="col-lg-5">
<div class="banner-content">
<h1>Nike New <br>Collection!</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
<div class="add-bag d-flex align-items-center">
<a class="add-btn" href="#"><span class="lnr lnr-cross"></span></a>
<span class="add-text text-uppercase">Add to Bag</span>
</div>
</div>
</div>
<div class="col-lg-7">
<div class="banner-img">
<img class="img-fluid" src="<?php echo base_url(); ?>assets/template-depan/img/banner/banner-img.png" alt="">
</div>
</div>
</div> -->
</div>
</div>
</div>
</div>
</section>

<!-- GGWP -->
<section >

<div class="row justify-content-center align-items-center">
<div class="col-lg-4 no-padding exclusive-left">
  <div class="item">
    <img src="<?php echo base_url(); ?>assets/template-depan/img/category/shoesbg.jpg" alt="pepsi">
    <div class="item-overlay top"><h2 style="color: white; padding-top: 10%;">Shoes</h2>
    <br><br>
    <a href="#" class="primary-btn">GO</a></div>
  </div>
</div>
<div class="col-lg-4 no-padding exclusive-left">
  <div class="item">
    <img src="<?php echo base_url(); ?>assets/template-depan/img/category/bagbg.jpg" alt="pepsi">
    <div class="item-overlay top"><h2 style="color: white; padding-top: 10%;">Bag</h2>
    <br><br>
    <a href="#" class="primary-btn">GO</a></div>
  </div>
</div>
<div class="col-lg-4 no-padding exclusive-left">
  <div class="item">
    <img src="<?php echo base_url(); ?>assets/template-depan/img/category/c2.jpg" alt="pepsi">
    <div class="item-overlay top"><h2 style="color: white; padding-top: 10%;">Special Offer</h2>
    <br><br>
    <a href="#" class="primary-btn">GO</a></div>
  </div>
</div>
</div>
</section>

<!-- Services -->
<section class="features-area section_gap">
<div class="container">
<div class="row features-inner">

<div class="col-lg-3 col-md-6 col-sm-6">
<div class="single-features">
<div class="f-icon">
<img src="<?php echo base_url(); ?>assets/template-depan/img/features/f-icon1.png" alt="">
</div>
<h6>Free Delivery</h6>
<p>Free Shipping on all order</p>
</div>
</div>

<div class="col-lg-3 col-md-6 col-sm-6">
<div class="single-features">
<div class="f-icon">
<img src="<?php echo base_url(); ?>assets/template-depan/img/features/f-icon2.png" alt="">
</div>
<h6>Return Policy</h6>
<p>Free Shipping on all order</p>
</div>
</div>

<div class="col-lg-3 col-md-6 col-sm-6">
<div class="single-features">
<div class="f-icon">
<img src="<?php echo base_url(); ?>assets/template-depan/img/features/f-icon3.png" alt="">
</div>
<h6>24/7 Support</h6>
<p>Free Shipping on all order</p>
</div>
</div>

<div class="col-lg-3 col-md-6 col-sm-6">
<div class="single-features">
<div class="f-icon">
<img src="<?php echo base_url(); ?>assets/template-depan/img/features/f-icon4.png" alt="">
</div>
<h6>Secure Payment</h6>
<p>Free Shipping on all order</p>
</div>
</div>
</div>
</div>
</section>



<section class="exclusive-deal-area">
<div class="container-fluid">
<div class="row justify-content-center align-items-center">
<div class="col-lg-12 no-padding exclusive-left">
<div class="row clock_sec clockdiv" id="clockdiv">
<div class="col-lg-12">
<h1>Exclusive Hot Deal Ends Soon!</h1>
<p>Who are in extremely love with eco friendly system.</p>
</div>
<div class="col-lg-12">
<div class="row clock-wrap">
<div class="col clockinner1 clockinner">
<h1 class="days">150</h1>
<span class="smalltext">Days</span>
</div>
<div class="col clockinner clockinner1">
<h1 class="hours">23</h1>
<span class="smalltext">Hours</span>
</div>
<div class="col clockinner clockinner1">
<h1 class="minutes">47</h1>
<span class="smalltext">Mins</span>
</div>
<div class="col clockinner clockinner1">
<h1 class="seconds">59</h1>
<span class="smalltext">Secs</span>
</div>
</div>
</div>
</div>
<a href="#" class="primary-btn">Shop Now</a>
</div>

</section>


<section class="brand-area section_gap">
<div class="container">
<div class="row">
<a class="col single-img" href="#">
<img class="img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/template-depan/img/brand/1.png" alt="">
</a>
<a class="col single-img" href="#">
<img class="img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/template-depan/img/brand/2.png" alt="">
</a>
<a class="col single-img" href="#">
<img class="img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/template-depan/img/brand/6.png" alt="">
</a>
<a class="col single-img" href="#">
<img class="img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/template-depan/img/brand/4.png" alt="">
</a>
<a class="col single-img" href="#">
<img class="img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/template-depan/img/brand/5.png" alt="">
</a>
</div>
</div>
</section>

<!-- 
<section class="related-product-area section_gap_bottom">
<div class="container">
<div class="row justify-content-center">
<div class="col-lg-6 text-center">
<div class="section-title">
<h1>Deals of the Week</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
magna aliqua.</p>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-9">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-6 mb-20">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r1.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
<h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 mb-20">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r2.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
 <h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 mb-20">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r3.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
<h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 mb-20">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r5.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
<h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 mb-20">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r6.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
<h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 mb-20">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r7.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
<h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r9.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
<h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r10.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
<h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="single-related-product d-flex">
<a href="#"><img src="<?php echo base_url(); ?>assets/template-depan/img/r11.jpg" alt=""></a>
<div class="desc">
<a href="#" class="title">Black lace Heels</a>
<div class="price">
<h6>$189.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3">
<div class="ctg-right">
<a href="#" target="_blank">
<img class="img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/template-depan/img/category/c5.jpg" alt="">
</a>
</div>
</div>
</div>
</div>
</section>

 -->