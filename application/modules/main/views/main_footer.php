


<script type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/template-depan/js/vendor/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/vendor/bootstrap.min.js"type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/jquery.ajaxchimp.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/jquery.nice-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/jquery.sticky.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/nouislider.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/countdown.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/jquery.magnific-popup.min.js" ttype="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/owl.carousel.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/template-depan/js/gmaps.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/template-depan/js/main.js" type="text/javascript"></script>

<script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</html>
<footer class="footer-area section_gap">
<div class="container">
<div class="row">
	<div class="col-md-4 col-sm-6">
		<div class="single-footer-widget">
		<h6>About Us</h6>
		<p>
		Authentic Shoes
		</p>
	</div>
</div>
<div class="col-md-4 col-sm-6">
	<div class="single-footer-widget">
		<h6>Follow Us</h6>
		<p>Let us be social</p>
		<div class="footer-social d-flex align-items-center">
		<a href="#"><i class="fa fa-facebook"></i></a>
		<a href="#"><i class="fa fa-twitter"></i></a>
		<a href="#"><i class="fa fa-dribbble"></i></a>
		<a href="#"><i class="fa fa-behance"></i></a>
	</div>
</div>
</div>
<div class="col-md-4 col-sm-6">
	<div class="single-footer-widget">
	<button type="button" class="primary-btn" data-toggle="modal" data-target="#exampleModal">Feedback<i class="mdi mdi-play-circle ml-1"></i></button>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document"> 
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">FEEDBACK</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button> 
		</div>
<div class="modal-body">
	<label for="fname">First Name
		<input type="text" id="fname" name="firstname" placeholder="Your name..">
		<label for="lname">Last Name</label>
		<input type="text" id="lname" name="lastname" placeholder="Your last name..">
	<label for="subject">Subject</label>
	<textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="primary-btn">Submit</button>
	<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
</div>
</div>
</div>
</div>
		</p>
	</div>
</div>
</div>
</div>

<br>
<br>


<div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
<p class="footer-text m-0">
Copyright &copy;<script type="be8453bdac35f61a028f94fe-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by Centree

</p>

</footer>




<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color:
  padding: 20px;
}
</style>
