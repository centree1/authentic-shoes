<style>
/* 
when .item:hover, show overlay.
overwrite .item-overlay."position"
*/
.item:hover .item-overlay.top {
  top: 0;
}
.item:hover .item-overlay.right {
  right: 0;
  left: 0;
}
.item:hover .item-overlay.bottom {
  bottom: 0;
}
.item:hover .item-overlay.left {
  left: 0;
}

/* 
by default, overlay is visible… 
*/
.item-overlay {
  position: absolute;
  top: 0; right: 0; bottom: 0; left: 0;
  
  background: rgba(0,0,0,0.5);
  color: #fff;
  overflow: hidden;
  text-align: center;
  /* fix text transition issue for .left and .right but need to overwrite left and right properties in .right */
  width: 100%; 
  
  -moz-transition: top 0.3s, right 0.3s, bottom 0.3s, left 0.3s;
  -webkit-transition: top 0.3s, right 0.3s, bottom 0.3s, left 0.3s;
  transition: top 0.3s, right 0.3s, bottom 0.3s, left 0.3s;
}
/*
…but this hide it
*/
.item-overlay.top {
  top: 100%;
}
.item-overlay.right {
  right: 200%;
  left: -100%;
}
.item-overlay.bottom {
  bottom: 100%;
}
.item-overlay.left {
  left: 100%;
}


/* misc. CSS */
* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

.item {
  position: relative;
  
  
  float: left;

  overflow: hidden;

  max-width: 540px;
}
.item img {
  max-width: 100%;
}
/* end of misc. CSS */

.card {
  /* Add shadows to create the "card" effect */
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

/* Add some padding inside the card container */
.container {
  padding: 2px 16px;
}

.card {
  box-shadow: 0 0px 0px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  border-radius: 2px; /* 5px rounded corners */

}

.carousel-item {
  height: 30vh;
  min-height: 250px;
  background: no-repeat center center scroll;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  background-color: #EBEBEB;
}

/* Add rounded corners to the top left and the top right corner of the image */
img {
  border-radius: 2px 2px 0 0;
}


</style>



<section class="banner-area organic-breadcrumb">
<div class="container">
<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
<div class="col-first">
<h1>Shop Category page</h1>
<!-- <nav class="d-flex align-items-center">
<a href="index-2.html">Home<span class="lnr lnr-arrow-right"></span></a>
<a href="#">Katalog -->
</nav>
</div>
</div>
</div>
</section>
<!-- 
<div class="container">
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12"> -->
<!-- <div class="sidebar-categories">
<div class="head">Browse Categories</div>
<ul class="main-categories">
<li class="main-nav-list"><a data-toggle="collapse" href="#fruitsVegetable" aria-expanded="false" aria-controls="fruitsVegetable"><span class="lnr lnr-arrow-right"></span>Fruits and Vegetables<span class="number">(53)</span></a>
<ul class="collapse" id="fruitsVegetable" data-toggle="collapse" aria-expanded="false" aria-controls="fruitsVegetable">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a data-toggle="collapse" href="#meatFish" aria-expanded="false" aria-controls="meatFish"><span class="lnr lnr-arrow-right"></span>Meat and Fish<span class="number">(53)</span></a>
<ul class="collapse" id="meatFish" data-toggle="collapse" aria-expanded="false" aria-controls="meatFish">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a data-toggle="collapse" href="#cooking" aria-expanded="false" aria-controls="cooking"><span class="lnr lnr-arrow-right"></span>Cooking<span class="number">(53)</span></a>
<ul class="collapse" id="cooking" data-toggle="collapse" aria-expanded="false" aria-controls="cooking">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a data-toggle="collapse" href="#beverages" aria-expanded="false" aria-controls="beverages"><span class="lnr lnr-arrow-right"></span>Beverages<span class="number">(24)</span></a>
<ul class="collapse" id="beverages" data-toggle="collapse" aria-expanded="false" aria-controls="beverages">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a data-toggle="collapse" href="#homeClean" aria-expanded="false" aria-controls="homeClean"><span class="lnr lnr-arrow-right"></span>Home and Cleaning<span class="number">(53)</span></a>
<ul class="collapse" id="homeClean" data-toggle="collapse" aria-expanded="false" aria-controls="homeClean">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a href="#">Pest Control<span class="number">(24)</span></a></li>
<li class="main-nav-list"><a data-toggle="collapse" href="#officeProduct" aria-expanded="false" aria-controls="officeProduct"><span class="lnr lnr-arrow-right"></span>Office Products<span class="number">(77)</span></a>
<ul class="collapse" id="officeProduct" data-toggle="collapse" aria-expanded="false" aria-controls="officeProduct">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a data-toggle="collapse" href="#beauttyProduct" aria-expanded="false" aria-controls="beauttyProduct"><span class="lnr lnr-arrow-right"></span>Beauty Products<span class="number">(65)</span></a>
<ul class="collapse" id="beauttyProduct" data-toggle="collapse" aria-expanded="false" aria-controls="beauttyProduct">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a data-toggle="collapse" href="#healthProduct" aria-expanded="false" aria-controls="healthProduct"><span class="lnr lnr-arrow-right"></span>Health Products<span class="number">(29)</span></a>
<ul class="collapse" id="healthProduct" data-toggle="collapse" aria-expanded="false" aria-controls="healthProduct">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
 <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a href="#">Pet Care<span class="number">(29)</span></a></li>
<li class="main-nav-list"><a data-toggle="collapse" href="#homeAppliance" aria-expanded="false" aria-controls="homeAppliance"><span class="lnr lnr-arrow-right"></span>Home Appliances<span class="number">(15)</span></a>
<ul class="collapse" id="homeAppliance" data-toggle="collapse" aria-expanded="false" aria-controls="homeAppliance">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
<li class="main-nav-list"><a class="border-bottom-0" data-toggle="collapse" href="#babyCare" aria-expanded="false" aria-controls="babyCare"><span class="lnr lnr-arrow-right"></span>Baby Care<span class="number">(48)</span></a>
<ul class="collapse" id="babyCare" data-toggle="collapse" aria-expanded="false" aria-controls="babyCare">
<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
<li class="main-nav-list child"><a href="#" class="border-bottom-0">Meat<span class="number">(11)</span></a></li>
</ul>
</li>
</ul>
</div>
<div class="sidebar-filter mt-50">
<div class="top-filter-head">Product Filters</div>
<div class="common-filter">
<div class="head">Brands</div>
<form action="#">
<ul>
<li class="filter-list"><input class="pixel-radio" type="radio" id="apple" name="brand"><label for="apple">Apple<span>(29)</span></label></li>
<li class="filter-list"><input class="pixel-radio" type="radio" id="asus" name="brand"><label for="asus">Asus<span>(29)</span></label></li>
<li class="filter-list"><input class="pixel-radio" type="radio" id="gionee" name="brand"><label for="gionee">Gionee<span>(19)</span></label></li>
<li class="filter-list"><input class="pixel-radio" type="radio" id="micromax" name="brand"><label for="micromax">Micromax<span>(19)</span></label></li>
<li class="filter-list"><input class="pixel-radio" type="radio" id="samsung" name="brand"><label for="samsung">Samsung<span>(19)</span></label></li>
</ul>
</form>
</div>
<div class="common-filter">
<div class="head">Color</div>
<form action="#">
<ul>
<li class="filter-list"><input class="pixel-radio" type="radio" id="black" name="color"><label for="black">Black<span>(29)</span></label></li>
<li class="filter-list"><input class="pixel-radio" type="radio" id="balckleather" name="color"><label for="balckleather">Black
Leather<span>(29)</span></label></li>
<li class="filter-list"><input class="pixel-radio" type="radio" id="blackred" name="color"><label for="blackred">Black
with red<span>(19)</span></label></li>
<li class="filter-list"><input class="pixel-radio" type="radio" id="gold" name="color"><label for="gold">Gold<span>(19)</span></label></li>
<li class="filter-list"><input class="pixel-radio" type="radio" id="spacegrey" name="color"><label for="spacegrey">Spacegrey<span>(19)</span></label></li>
</ul>
</form>
</div>
<div class="common-filter">
<div class="head">Price</div>
<div class="price-range-area">
<div id="price-range"></div>
<div class="value-wrapper d-flex">
<div class="price">Price:</div>
<span>$</span>
<div id="lower-value"></div>
<div class="to">to</div>
<span>$</span>
<div id="upper-value"></div>
</div>
</div>
</div>
</div>
</div> -->
<!-- <div class="col-xl-12 col-lg-12 col-md-12">

<div class="filter-bar d-flex flex-wrap align-items-center">
<div class="sorting">
<select>
<option value="1">Default sorting</option>
<option value="1">Default sorting</option>
<option value="1">Default sorting</option>
</select>
</div>
<div class="sorting mr-auto">
<select>
<option value="1">Show 12</option>
<option value="1">Show 12</option>
<option value="1">Show 12</option>
</select>
</div>
<div class="pagination">
<a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
<a href="#" class="active">1</a>
<a href="#">2</a>
<a href="#">3</a>
<a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
<a href="#">6</a>
<a href="#" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
</div>
</div>
 -->
<!-- GGWP -->


<section >

<div class="row justify-content-center align-items-center">
<div class="col-lg-3 no-padding exclusive-left">
  <div class="item">
    <img src="<?php echo base_url(); ?>assets/template-depan/img/category/gg1.jpg" alt="pepsi">
    <div class="item-overlay top"><h2 style="color: white; padding-top: 30%;">Stylish</h2>
    <br><br>
    <a href="#" class="primary-btn">GO</a></div>
  </div>
</div>
<div class="col-lg-3 no-padding exclusive-left">
  <div class="item">
    <img src="<?php echo base_url(); ?>assets/template-depan/img/category/gg2.jpg" alt="pepsi">
    <div class="item-overlay top"><h2 style="color: white; padding-top: 30%;">Man</h2>
    <br><br>
    <a href="#" class="primary-btn">GO</a></div>
  </div>
</div>
<div class="col-lg-3 no-padding exclusive-left">
  <div class="item">
    <img src="<?php echo base_url(); ?>assets/template-depan/img/category/gg3.jpg" alt="pepsi">
    <div class="item-overlay top"><h2 style="color: white; padding-top: 30%;">Woman</h2>
    <br><br>
    <a href="#" class="primary-btn">GO</a></div>
  </div>
</div>
<div class="col-lg-3 no-padding exclusive-left">
  <div class="item">
    <img src="<?php echo base_url(); ?>assets/template-depan/img/category/gg4.jpg" alt="pepsi">
    <div class="item-overlay top"><h2 style="color: white; padding-top: 30%;">Kids</h2>
    <br><br>
    <a href="#" class="primary-btn">GO</a></div>
  </div>
</div>
</div>

<!-- 
<div class="container">
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12">
<section class="lattest-product-area pb-40 category-list">
<div class="row">
<div class="col-lg-4 col-md-6">
<div class="single-product">

<img class="img-fluid" src="<?php echo base_url(); ?>assets/template-depan/img/product/p1.jpg" alt="">

<div class="product-details">

<h6>addidas New Hammer sole
for Sports person</h6>
<div class="price">
<h6>$150.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
<div class="prd-bottom">
<a href="#" class="social-info">
<span class="ti-bag"></span>
<p class="hover-text">add to bag</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-heart"></span>
<p class="hover-text">Wishlist</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-sync"></span>
<p class="hover-text">compare</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-move"></span>
<p class="hover-text">view more</p>
</a>
</div>
</div>
</div>
</div>

<div class="col-lg-4 col-md-6">
<div class="single-product">
<img class="img-fluid" src="<?php echo base_url(); ?>assets/template-depan/img/product/p2.jpg" alt="">
<div class="product-details">
<h6>addidas New Hammer sole
for Sports person</h6>
<div class="price">
<h6>$150.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
<div class="prd-bottom">
<a href="#" class="social-info">
<span class="ti-bag"></span>
<p class="hover-text">add to bag</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-heart"></span>
<p class="hover-text">Wishlist</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-sync"></span>
<p class="hover-text">compare</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-move"></span>
<p class="hover-text">view more</p>
</a>
</div>
</div>
</div>
</div>

<div class="col-lg-4 col-md-6">
<div class="single-product">
<img class="img-fluid" src="<?php echo base_url(); ?>assets/template-depan/img/product/p3.jpg" alt="">
<div class="product-details">
<h6>addidas New Hammer sole
for Sports person</h6>
<div class="price">
<h6>$150.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
<div class="prd-bottom">
<a href="#" class="social-info">
<span class="ti-bag"></span>
<p class="hover-text">add to bag</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-heart"></span>
<p class="hover-text">Wishlist</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-sync"></span>
<p class="hover-text">compare</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-move"></span>
<p class="hover-text">view more</p>
</a>
</div>
</div>
</div>
</div>

<div class="col-lg-4 col-md-6">
<div class="single-product">
<img class="img-fluid" src="<?php echo base_url(); ?>assets/template-depan/img/product/p4.jpg" alt="">
<div class="product-details">
<h6>addidas New Hammer sole
for Sports person</h6>
<div class="price">
<h6>$150.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
<div class="prd-bottom">
<a href="#" class="social-info">
<span class="ti-bag"></span>
<p class="hover-text">add to bag</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-heart"></span>
<p class="hover-text">Wishlist</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-sync"></span>
<p class="hover-text">compare</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-move"></span>
<p class="hover-text">view more</p>
</a>
</div>
</div>
</div>
</div>

<div class="col-lg-4 col-md-6">
<div class="single-product">
<img class="img-fluid" src="<?php echo base_url(); ?>assets/template-depan/img/product/p5.jpg" alt="">
<div class="product-details">
<h6>addidas New Hammer sole
for Sports person</h6>
<div class="price">
<h6>$150.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
<div class="prd-bottom">
<a href="#" class="social-info">
<span class="ti-bag"></span>
<p class="hover-text">add to bag</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-heart"></span>
<p class="hover-text">Wishlist</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-sync"></span>
<p class="hover-text">compare</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-move"></span>
<p class="hover-text">view more</p>
</a>
</div>
</div>
</div>
</div>

<div class="col-lg-4 col-md-6">
<div class="single-product">
<img class="img-fluid" src="<?php echo base_url(); ?>assets/template-depan/img/product/p6.jpg" alt="">
<div class="product-details">
<h6>addidas New Hammer sole
for Sports person</h6>
<div class="price">
<h6>$150.00</h6>
<h6 class="l-through">$210.00</h6>
</div>
<div class="prd-bottom">
<a href="#" class="social-info">
<span class="ti-bag"></span>
<p class="hover-text">add to bag</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-heart"></span>
<p class="hover-text">Wishlist</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-sync"></span>
<p class="hover-text">compare</p>
</a>
<a href="#" class="social-info">
<span class="lnr lnr-move"></span>
<p class="hover-text">view more</p>
</a>
</div>
 </div>
</div>
</div>
</div>
</section>
  -->
  <br><br>
<section >
<center>
<div>
  <h2>NEW PRODUCT</h2>
</div>

<div>
  <img src="<?php echo base_url(); ?>assets/template-depan/img/product/linee.png" alt="">
</div>
</center>
</section>

  <br>
<section >
<div class="row justify-content-center align-items-center">
<!-- Card lama -->
<!-- <div class="col-lg-2 no-padding exclusive-left">
<div class="card">
<div class="item">
  <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p2.jpg" alt="">
  <div class="item-overlay top">
  <div>
    <br><br><br><br>
    <a href="#" class="primary-btn">View More</a>
  </div>
  </div>
</div>
  <div class="container">
   </style><h4><b>Shoes</b></h4>
    <p>Sneaker</p>
  </div>
</div>
</div> -->

<div class="col-lg-2 no-padding exclusive-left">
<div class="card">
<div class="item">
  <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p1.jpg" alt="">
  <div class="item-overlay top">
  <div>
    <br><br><br>
    <a href="#" class="primary-btn" data-toggle="modal" data-target="#exampleModall">View More</a><i class="mdi mdi-play-circle ml-1"></i>
    <br>
    <br>
    <a href="#" class="primary-btn" data-toggle="modal" data-target="#exampleModalll">BUY</a><i class="mdi mdi-play-circle ml-1"></i>
  </div>
  </div>
</div>
</div>
<br>
 <center><h5><b>Avid Sneakers - Gray</b></h5>
    <p>Rp. 450.000</p></center>
</div>

<div class="col-lg-2 no-padding exclusive-left">
<div class="card">
<div class="item">
  <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p2.jpg" alt="">
  <div class="item-overlay top">
  <div>
    <br><br><br>
    <a href="#" class="primary-btn">View More</a>
    <br>
    <br>
    <a href="#" class="primary-btn">BUY</a>
  </div>
  </div>
</div>
</div>
<br>
 <center><h5><b>Gamble Sneakers - Colorful</b></h5>
    <p>Rp. 280.000</p></center>
</div>

<div class="col-lg-2 no-padding exclusive-left">
<div class="card">
<div class="item">
  <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p3.jpg" alt="">
  <div class="item-overlay top">
  <div>
    <br><br><br>
    <a href="#" class="primary-btn">View More</a>
    <br>
    <br>
    <a href="#" class="primary-btn">BUY</a>
  </div>
  </div>
</div>
</div>
<br>
 <center><h5><b>Misty Sneakers - Black</b></h5>
    <p>Rp. 250.000</p></center>
</div>

<div class="col-lg-2 no-padding exclusive-left">
<div class="card">
<div class="item">
  <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p4.jpg" alt="">
  <div class="item-overlay top">
  <div>
    <br><br><br>
    <a href="#" class="primary-btn">View More</a>
    <br>
    <br>
    <a href="#" class="primary-btn">BUY</a>
  </div>
  </div>
</div>
</div>
<br>
 <center><h5><b>Nera Sneakers - Yellow</b></h5>
    <p>Rp. 320.000</p></center>
</div>

<div class="col-lg-2 no-padding exclusive-left">
<div class="card">
<div class="item">
  <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p5.jpg" alt="">
  <div class="item-overlay top">
  <div>
    <br><br><br>
    <a href="#" class="primary-btn">View More</a>
    <br>
    <br>
    <a href="#" class="primary-btn">BUY</a>
  </div>
  </div>
</div>
</div>
<br>
 <center><h5><b>Kygo Sneakers - Gray</b></h5>
    <p>Rp. 540.000</p></center>
</div>

<div class="col-lg-2 no-padding exclusive-left">
<div class="card">
<div class="item">
  <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p6.jpg" alt="">
  <div class="item-overlay top">
  <div>
    <br><br><br>
    <a href="#" class="primary-btn">View More</a>
    <br>
    <br>
    <a href="#" class="primary-btn">BUY</a>
  </div>
  </div>
</div>
</div>
<br>
 <center><h5><b>Kenya Sneakers - Orange</b></h5>
    <p>Rp. 670.000</p></center>
</div>
</section>



<br>

<div class="filter-bar d-flex flex-wrap align-items-center">
	<div class="sorting mr-auto">
		<select>
		<option value="1">Show 10</option>
		<option value="1">Show 15</option>
		<option value="1">Show 20</option>
		</select>
	</div>
<div class="pagination">
	<a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
	<a href="#" class="active">1</a>
	<a href="#">2</a>
	<a href="#">3</a>
	<a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
	<a href="#">6</a>
	<a href="#" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
</div>
</div>




<!-- KONTEN MODAL VIEW MORE-->
<div class="modal fade" id="exampleModall" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document"> 
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">DETAIL PRODUK</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button> 
    </div>

<div class="modal-body">
  <div class="row">
  <div class="col-md-12">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active">
       <center> <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p1.jpg" alt=""></center>
        <div class="carousel-caption d-none d-md-block">
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item">
         <center> <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p1.jpg" alt=""></center>
        <div class="carousel-caption d-none d-md-block">
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item">
         <center> <img src="<?php echo base_url(); ?>assets/template-depan/img/product/p1.jpg" alt=""></center>
        <div class="carousel-caption d-none d-md-block">
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" >
          <span class="carousel-control-prev-icon" aria-hidden="true" ></span>
          <span class="sr-only" >Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>
  <br>
     <center><h5><b>Avid Sneakers - Gray</b></h5>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- KONTEN MODAL BUY-->
<div class="modal fade" id="exampleModalll" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document"> 
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">SILAHKAN ISI DATA PEMBELANJAAN</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button> 
    </div>

<div class="modal-body">
  <div class="row">
  <div class="col-md-6">
  <center><img src="<?php echo base_url(); ?>assets/template-depan/img/product/p1.jpg" alt=""></center>
  <center><h5><b>Avid Sneakers - Gray</b></h5>
  <p>Rp. 450.000</p></center>

  </div>
<br><br>
  <div class="col-md-6">
  <label for="freceiver">Nama Penerima</label>
    <input type="text" id="freceiver" name="penerima" placeholder="Customer Name">
    <label for="ltelp">Nomor Telepon</label>
    <input type="text" id="ltelp" name="telepon" placeholder="Mobile Number">
    <label for="lalamat">Alamat</label>
    <textarea id="subject" name="lalamat" placeholder="Address" style="height:200px"></textarea>
    <label for="lpropinsi">Propinsi</label>
    <input type="text" id="lpropinsi" name="propinsi" placeholder="">
    <label for="lkota">Kota</label>
    <input type="text" id="lkota" name="kota" placeholder="">
    <label for="lkecamatan">Kecamatan</label>
    <input type="text" id="lkecamatan" name="kecamatan" placeholder="">
    <label for="lkodepos">Kode Pos</label>
    <input type="text" id="lkodepos" name="postalcode" placeholder="Postal Code">
    <br>
    <label for="ljumlah">Jumlah Barang</label><p>
    <input type="number" id="ljumlah" name="jumlahbarang" placeholder=""></p>
    <br>
    <h5>Sub Total : Rp. 450.000</h5>'
    <br>
  </form>
</div>
</div>
<div class="modal-footer">
  <button type="button" class="primary-btn">Submit</button>
  <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
</div>
</div>
</div>
</div>
    </p>
  </div>
</div>
</div>
</div>


