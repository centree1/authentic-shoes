<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/authentic-shoes2.jpeg">

<meta name="author" content="CodePixar">

<meta name="description" content="">

<meta name="keywords" content="">

<meta charset="UTF-8">

<title><?php echo $titlexyz; ?></title>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/linearicons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/nouislider.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/ion.rangeSlider.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/ion.rangeSlider.skinFlat.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-depan/css/main.css">
</head>
<body>

<header class="header_area sticky-header">
<div class="main_menu">
<nav class="navbar navbar-expand-lg navbar-light main_box">
<div class="container">

<a class="navbar-brand logo_h" href="main"><img src="<?php echo base_url(); ?>assets/images/authentic-shoes2.jpeg" alt="" ></a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>

<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
<ul class="nav navbar-nav menu_nav ml-auto">

<?php $this->load->view('main_menu'); ?>

</ul>
</div>
</div>
</nav>
</div>
<!-- <div class="search_input" id="search_input_box">
<div class="container">
<form class="d-flex justify-content-between">
<input type="text" class="form-control" id="search_input" placeholder="Search Here">
<button type="submit" class="btn"></button>
<span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
</form>
</div>
</div> -->
</header>